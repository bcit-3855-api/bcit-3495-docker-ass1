import mysql.connector
from apscheduler.schedulers.background import BackgroundScheduler
import logging, logging.config
import yaml, time
from pymongo import MongoClient

log_config_file = "log_conf.yml"
with open(log_config_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

connection = mysql.connector.connect(
    host='db',
    user='username',
    password='password',
    database='mydatabase'
)

cursor = connection.cursor()

# MongoDB setup
mongo_client = MongoClient('mongodb://host.docker.internal:27017/')
mongo_db = mongo_client['weather-storage']
mongo_collection = mongo_db['weather-information']

def getAddresses():
    cursor.execute('''select userid, country, province, city, street, postal from Addresses;''')
    return cursor.fetchall()

def getUsers():
    cursor.execute('''select userid, username from Users;''')
    return cursor.fetchall()

def update_mongo():
    dont_update = []
    to_update = []
    addresses = getAddresses()
    users = getUsers()
    # Fetch all documents from MongoDB collection
    all_documents = mongo_collection.find()
    for document in all_documents:
        print(document)
        mongo_address = tuple(document['user_address'].values())
        for address in addresses:
            if address == mongo_address:
                dont_update.append(address)
                break
    for address in addresses:
        if address not in dont_update:
            to_update.append(address)
    
    for record in to_update:
        for user in users:
            if user[0] == record[0]:
                data = {
                "username": user[1],
                "user_address": {"userid": user[0], "country": record[1], "province": record[2], "city": record[3], "street": record[4], "postal": record[5]},
                'Current Weather Report': 'Sunny',
                'Weather Report for Upcoming Days': [
                    {"day": 'Monday', "conditions": 'Cloudy', "temperature": 24, "Time": '2022-02-15T12:00:00.000Z'},
                    {"day": 'Tuesday', "conditions": 'Rainy', "temperature": 20, "Time": '2022-02-15T12:00:00.000Z'}
                ]
                }

        mongo_collection.insert_one(data)
        
        
        

def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(update_mongo, 'interval', seconds=5)
    sched.start()

if __name__ == "__main__":
    init_scheduler()
    while True:
        time.sleep(1)
