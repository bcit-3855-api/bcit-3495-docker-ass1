from flask import Flask, Response, render_template, request, jsonify
import mysql.connector
from flask_cors import CORS

connection = mysql.connector.connect(
    host='db',
    user='username',
    password='password',
    database='mydatabase'
)

cursor = connection.cursor()

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
def login():
    return render_template('login.html')

@app.route('/login', methods=['GET'])
def login2():
    return render_template('login.html')

@app.route('/enter', methods=['GET'])
def enter():
    return render_template('enter.html')

@app.route('/data', methods=['POST'])
def data():
    data = request.json
    country = data['country']
    province = data['province']
    city = data['city']
    postal = data['postal']
    street = data['street']
    userid = data['userid']

    cursor.execute('''insert into Addresses (userid, country, province, city, street, postal)
                   values (%s, %s, %s, %s, %s, %s)''', (userid, country, province, city, street, postal))
    connection.commit()
    
    return render_template('enter.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)