
const form = document.getElementById('myForm');

form.addEventListener('submit', function(event) {
    event.preventDefault();
});

document.getElementById("subButton").addEventListener("click", event => {
    var country = document.getElementById("country").value;
    var province = document.getElementById("province").value;
    var city = document.getElementById("city").value;
    var street = document.getElementById("street").value;
    var postal = document.getElementById("postal").value;
    var userid = document.cookie.replace(/(?:(?:^|.*;\s*)userid\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/data");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    const body = JSON.stringify({
        country: country,
        province: province,
        city: city,
        street, street,
        postal: postal,
        userid: userid
    });
    
    xhr.onload = () => {
        if (xhr.readyState == 4 && xhr.status == 201) {
            document.cookie = xhr.responseText;
        } else {
            console.log(`Error: ${xhr.status}`);
        }
    };
    xhr.send(body);
})