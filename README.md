# Technical Report: Weather App Development
# Developed By: Munib Javed, Adrian Balcerak

## 1. Executive Summary

Welcome to our Dockerized Weather App – a small project with big Docker vibes! In this microservices-driven weather experience, we've got a simple setup handled by just two coding enthusiasts, Munib Javed and Adrian Balcerak.

### What We've Got:

- **Enter Data Service:** Think of it as our data input wizard. You enter any data after a quick stop at our Authentication Service, and voila! It's stored in our MySQL DB.

- **Show Results Service:** Ever wondered about simple analytics like max, min, and average? Our Show Results Service has got your back. Validate through the Authentication Service and enjoy the insights fetched from our MongoDB.

- **Authentication Service:** The gatekeeper. Ensures your credentials are on the guest list for both Enter Data and Show Results services.

- **Analytics Service:** Behind the scenes, this service does some MySQL magic, computes simple stats, and writes them to our MongoDB.

So, why Docker? It's our secret sauce for scalability and easy maintenance. Join us in this Dockerized adventure for a weather app that's more than just a side project – it's our coding journey! 🌦️⚙️🐳




## 2. Introduction

### 2.1 Project Background
We're building a weather app that's not just about checking the forecast—it's about you being part of the weather scene. Why settle for the usual weather updates when you can actively contribute your own data? Our app lets you log local weather conditions, throw in your preferences, and get cool personalized analytics.

Think of it as a community-driven weather hub, breaking the mold of regular weather apps. We want to go beyond just telling you if it's going to rain or shine. We're all about users sharing their weather experiences and insights.

And guess what? Behind the scenes, we're using some fancy tech – microservices architecture. It's not just a buzzword; it makes our app scalable, easy to maintain, and rock-solid. So, join us in creating a weather app that's more than just data – it's a shared experience crafted by the community, for the community. Let's Dockerize this weather vibe! 🌦️⚙️🐳


## 2.2 Objectives

For our class assignment focusing on Docker, our main objectives are to:

1. **Implement User Authentication:**
   - Develop a simple authentication service to validate user credentials.
   - Ensure secure handling of sensitive information, such as hashed passwords.

2. **Enable Data Entry (Enter Data Service):**
   - Create a web app for users to input weather-related data.
   - Validate user credentials through the Authentication Service before allowing data entry.

3. **Display Analytics (Show Results Service):**
   - Develop a web app to showcase basic analytics (e.g., max, min, average temperature).
   - Require user authentication via the Authentication Service to access analytics.

4. **Utilize Microservices Architecture:**
   - Implement microservices for Authentication, Enter Data, Show Results, and Analytics.
   - Leverage Docker to containerize each service, promoting scalability and maintainability.

5. **Manage Weather Data:**
   - Design the system to collect and store weather data efficiently.
   - Use MySQL for storing entered data and MongoDB for analytics results.

These objectives align with the scope of our assignment, emphasizing practical implementation and Docker utilization within a small-scale weather app project.



## 3. System Architecture

Our weather app is built upon a microservices architecture, leveraging Docker for efficient containerization of individual services. Below is an overview of the system components and their interactions:

### Microservices Overview:

1. **Authentication Service:**
   - Responsible for validating user credentials.
   - Ensures secure authentication before granting access to other services.

2. **Enter Data Service:**
   - Web app allowing users to input weather-related data.
   - Validates user credentials through the Authentication Service.
   - Writes entered data to the MySQL database.

3. **Show Results Service:**
   - Web app presenting basic analytics (max, min, average temperature).
   - Requires user authentication via the Authentication Service.
   - Reads data from the MongoDB database for showcasing results.

4. **Analytics Service:**
   - Computes simple statistics (Max, Min, Avg) from the MySQL database.
   - Writes analytics results to the MongoDB database.

### Interactions:

- **User Authentication Flow:**
  - Users must authenticate through the Authentication Service.
  - Upon successful authentication, they gain access to Enter Data and Show Results services.

- **Data Entry Flow:**
  - Users input weather data through the Enter Data Service web app.
  - The Authentication Service validates user credentials.
  - Entered data is written to the MySQL database.

- **Analytics Presentation Flow:**
  - Users access the Show Results Service web app.
  - Authentication Service validates user credentials.
  - Analytics results are retrieved from the MongoDB database for presentation.

### Diagram:

![Weather App Architecture](/Docker-Assignment1-Diagram.png)

This diagram illustrates the interaction between microservices, emphasizing the flow of data and authentication in our Dockerized weather app architecture.


## 4. Microservices Overview

## Authentication Service

The Authentication Service is a critical component responsible for handling user authentication. It is implemented as a web API using the Node Express framework. The service has configured CORS policies to allow requests from the designated web server. One of its primary functionalities is to provide a secure mechanism for validating user credentials.

**Authentication Workflow:**

- **Endpoint for Credentials:** The Authentication Service exposes an endpoint that receives user credentials.
- **Credential Validation:** It validates the received credentials, ensuring they are correct and secure.
- **Cookie Response:** Upon successful validation, the service responds by generating a secure cookie.
- **Authentication in Web Server:** The web server receives the cookie and sets it in the browser session, marking the user as authenticated.

This process ensures that users are securely authenticated, allowing them to interact with other microservices while maintaining a secure and controlled access environment.

## Enter Data Service

The Enter Data Service plays a pivotal role in enabling users to input weather-related data into the system. This service manages the reception, processing, and storage of user-submitted data into the MySQL database.

**Data Input Workflow:**

- **User Input:** Users submit weather-related data through the designated interface.
- **Processing:** The Enter Data Service processes the received data, ensuring its integrity and adherence to specified formats.
- **MySQL Database Storage:** Processed data is then securely stored in the MySQL database, ready for retrieval and analysis.

This service ensures the reliable and secure ingestion of data, contributing to the overall functionality of the weather app.

## Show Results Service

The Show Results Service focuses on presenting analytics and weather information to users based on the data retrieved from the MongoDB database. It acts as the interface through which users can access and visualize weather-related insights.

**Results Presentation:**

- **Retrieve Data:** The Show Results Service retrieves relevant weather data from the MongoDB database.
- **Analytics Processing:** It may perform additional analytics or computations on the retrieved data.
- **User Interface:** The processed information is presented to users through a user-friendly interface.

By offering a clear and concise presentation of analytics, this service enhances the user experience, providing valuable insights into weather patterns.

## Analytics Service

The Analytics Service is responsible for computing simple statistics from the MySQL database and storing the results in the MongoDB database. This service contributes to the overall data analysis and retrieval process, enhancing the app's ability to generate meaningful insights.

**Analytics Workflow:**

- **Retrieve Data:** The Analytics Service fetches relevant weather data from the MySQL database.
- **Computation:** It performs computations or statistical analyses on the retrieved data.
- **Storage in MongoDB:** The computed results are securely stored in the MongoDB database.

By offloading analytics tasks to a dedicated service, the app ensures efficient data processing and enables more advanced analytics capabilities.

## 5. Data Collection and Analytics

The weather app involves the collection of user-provided weather data and the performance of analytics to present meaningful insights. Here are the key aspects related to data collection, analytics, and associated considerations:

### 5.1 Data Collection

#### Enter Data Service
- **User Input:** Users input weather-related data, including location, temperature, current weather conditions, upcoming weather forecasts, and timestamps.
- **User Authentication:** To ensure data integrity, the Enter Data Service requires user authentication. The Authentication Service validates user credentials securely.

#### Privacy and User Consent
- **Privacy Measures:** The app adheres to privacy standards, ensuring the protection of user data.
- **Explicit Consent:** Users are required to provide explicit consent before submitting weather data, emphasizing transparency and compliance with privacy regulations.

### 5.2 Analytics

#### Analytics Service
- **Computational Analytics:** The Analytics Service computes simple statistics from the MySQL database, analyzing user-entered weather data.
- **Storage in MongoDB:** Analytical results are written to the MongoDB database for efficient retrieval and presentation in the Show Results Service.

#### Data Storage
- **MySQL Database:** User-entered weather data is stored securely in a MySQL database, ensuring reliability and data integrity.
- **MongoDB Database:** Analytical results are stored in a MongoDB database, providing a flexible and scalable solution for data retrieval.

### 5.3 Considerations
- **Data Security:** Robust security measures, including encryption and secure authentication, are implemented to protect user data.
- **Regulatory Compliance:** The app complies with data protection regulations, and user consent mechanisms are in place to meet privacy standards.

The data collection and analytics components are designed to provide users with accurate weather information while prioritizing privacy, security, and regulatory compliance.

## 6. Technologies Used

The development of the weather app involved the use of various technologies, including programming languages, frameworks, databases, and external APIs.

### 6.1 Programming Languages

- Python: Used with Flask to develop the main web application and data processing functionalities.
- JavaScript (Node.js): Utilized for creating the Authentication Service.

### 6.2 Web Frameworks

- Flask: Employed as the web framework for the main weather app, facilitating the creation of web pages and handling HTTP requests.
- Express.js: Utilized to develop the Authentication Service, providing a robust framework for building web applications with Node.js.

### 6.3 Databases

- MySQL: Integrated to store and manage weather-related data entered by users.
- MongoDB: Employed for storing and retrieving analytical data and results.

### 6.4 Containerization

- Docker: Used for containerizing various components of the weather app, ensuring consistency and ease of deployment.

### 6.5 Additional Technologies

- HTML/CSS: Utilized for designing and styling the user interface of the weather app.
- Docker Compose: Employed for orchestrating multiple containers, facilitating the deployment and management of the entire application.

### 6.6 External APIs

- No external API's were used in making of this application.


## 7. User Interface

### 7.1 Authentication Page

The Authentication Service provides a simple and secure login page. Users can enter their credentials, and upon successful authentication, a secure cookie is issued for subsequent sessions.

### 7.2 Enter Data Page

The Enter Data Service features an intuitive data input form. Users can easily input weather-related information, including location, temperature, current weather conditions, upcoming forecasts, and timestamp.

### 7.3 Show Results Page

The Show Results Service presents a clear and organized output page. Users can view requested weather information, including user details, temperature, current weather conditions, upcoming forecasts, and timestamp.


## 7.4. Images

### 7.4 Authentication Page

![Authentication Page](/Authenticate.png)

### 7.6 Enter Data Page

![Enter Data Page](/Enter-Data,png.png)

### 7.6 Show Results Page

![Show Results Page](/Output-results.png)


## 8. Challenges and Solutions

During the development of the weather app, Munib encountered several challenges that required creative problem-solving and collaboration within the team. Here's a breakdown of challenges and the solutions implemented:

### 8.1 Munib's Challenges and Solutions

#### MongoDB Setup and Syntax
- **Challenge:** Setting up MongoDB and understanding its syntax posed initial difficulties for Munib.
- **Solution:** Munib engaged in thorough research, leveraging online documentation and community forums. Collaborative efforts within the team and experimenting with MongoDB queries were key to overcoming this challenge.

#### Web Page Styling
- **Challenge:** Designing an aesthetically pleasing web page presented challenges in terms of CSS and styling.
- **Solution:** Munib delved into web design principles, explored CSS frameworks, and sought feedback from the team. Iterative improvements and collaborative design discussions contributed to achieving a visually appealing interface.

#### Docker Containerization
- **Challenge:** Containerizing the folders and ensuring the web app ran seamlessly within Docker was a learning curve.
- **Solution:** Munib actively engaged with Docker documentation, experimented with container configurations, and sought assistance from team members. Clearer understanding and step-by-step experimentation resulted in successful containerization.


#### Juggling Coursework and Development
- **Challenge:** Munib faced the challenge of balancing academic commitments, including coursework, with the demands of the weather app development project.
- **Solution:** Implementing effective time management strategies, Munib prioritized tasks and established a clear schedule. Regular communication with Adrian allowed for understanding each member's workload, enabling better collaboration and mutual support.

#### Striving for a Balance
- **Challenge:** Striking the right balance between academic responsibilities and project development became an ongoing challenge.
- **Solution:** Munib adopted a proactive approach, regularly reassessing priorities and adjusting schedules as needed. Open communication with Adrian and setting realistic expectations contributed to a harmonious workflow.


### 8.2 Adrian's Challenges and Solutions

#### CORS Policy Bypass
- **Challenge:** Circumventing the Cross-Origin Resource Sharing (CORS) policy presented a significant obstacle for Adrian, requiring a thorough exploration of CORS configurations to facilitate effective communication between the frontend and the authentication service.
- **Solution:** Adrian tackled the challenge by fine-tuning CORS settings, exploring alternative configurations, and ensuring seamless communication between different components.

#### Work-Life Balance
- **Challenge:** Juggling work commitments and academic responsibilities while actively contributing to the development of the weather app became a balancing act for Adrian.
- **Solution:** Adrian approached this challenge by setting clear priorities, managing time effectively, and maintaining open communication with Munib. Establishing a harmonious balance between work, academics, and project development contributed to a successful outcome.

#### Division of Coding Tasks
- **Challenge:** Dividing coding responsibilities efficiently between team members, with Adrian taking a larger portion of the coding section.
- **Solution:** The team embraced open communication and transparency in task allocation. Adrian actively collaborated with Munib to ensure a fair distribution of coding tasks while addressing individual strengths and preferences.

#### Balancing Learning and Execution
- **Challenge:** Munib focused on PPT, report writing, and overall project coordination, while Adrian delved into extensive coding tasks.
- **Solution:** The team fostered a collaborative learning environment. Adrian shared insights into coding intricacies, and Munib utilized Docker individually to enhance familiarity. Regular check-ins and knowledge-sharing sessions ensured a holistic understanding of both coding and project management aspects.


## 9. Future Enhancements

The weather app is a dynamic project that can be further enhanced to meet evolving user needs. Some potential areas for future improvements include:

### 9.1 Additional Features

- **Enhanced Analytics:** Implement more sophisticated analytics to provide users with deeper insights into weather patterns and trends.
- **User Preferences:** Introduce user preferences to allow customization of the app interface or receive personalized weather alerts.

### 9.2 Performance Improvements

- **Optimized Data Processing:** Explore optimizations in data processing and storage to enhance the app's overall performance.
- **Caching Mechanisms:** Implement caching mechanisms to reduce response times and improve user experience.

### 9.3 Scalability Considerations

- **Scalable Architecture:** Evaluate and implement a scalable architecture to accommodate an increasing number of users and data inputs.
- **Load Balancing:** Introduce load balancing strategies to distribute incoming traffic efficiently among multiple servers.

## 10. Conclusion

The development of the weather app has been a collaborative effort involving the use of various technologies and frameworks. Key outcomes and findings include:

- Successful integration of Python and Flask for the main web application, providing a user-friendly interface for weather-related interactions.
- Utilization of Node.js and Express.js for the Authentication Service, ensuring secure and seamless user authentication.
- Effective containerization with Docker, simplifying deployment and ensuring consistency across different components.
- Integration of MySQL and MongoDB databases to store and manage user-entered weather data and analytical results.

The weather app serves as a robust foundation for weather-related interactions, and continuous improvements will be made to enhance its functionality and user experience in the future.


## 11. References

1. **MongoDB Documentation:**
   - [Installation Guide](https://www.mongodb.com/docs/manual/installation/)

2. **DBVisualizer:**
   - [Containerizing MySQL with Docker and DBVisualizer](https://www.dbvis.com/thetable/containerizing-mysql-with-docker-and-dbvisualizer/#:~:text=To%20containerize%20a%20MySQL%20database,from%20it%20using%20Docker%20commands.)

3. **LinkedIn Article:**
   - [It's Always a CORS Problem: Troubleshooting and Solving Errors](https://www.linkedin.com/pulse/its-always-cors-problem-troubleshooting-solving-errors-carrubba-/)

4. **Free Frontend:**
   - [CSS Animated Backgrounds](https://freefrontend.com/css-animated-backgrounds/)

5. **CirrusLabs Blog:**
   - [How to Dockerize a Web Application](https://www.cirruslabs.io/blog1/modernized-technology/how-to-dockerize-an-web-application)
