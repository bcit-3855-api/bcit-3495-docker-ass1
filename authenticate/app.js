const express = require('express');
const app = express();
const port = 8090;

const mysql = require('mysql2');

const util = require('util');

const connection = mysql.createConnection({
  host: 'db',
  user: 'username',
  password: 'password',
  database: 'mydatabase'
});

const queryAsync = util.promisify(connection.query).bind(connection);

connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL server: ' + err.stack);
    return;
  }

  console.log('Connected to MySQL server as id ' + connection.threadId);
});

app.use(express.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8080');
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    res.sendStatus(200); // For preflight requests
} else {
    next();
}

})

app.post('/auth', async (req, res) => {
  username = req.body.username;
  password = req.body.password;
  var userid
  var authenticated
  
  const [results, fields] = await queryAsync('SELECT * FROM Users WHERE username = ? AND password = ?', [username, password]);
  if (results) {
    userid = results.userid ? results.userid : null;
    authenticated = userid ? true : false;
  } else {
    authenticated = false;
  }
  
  if (authenticated) {
    cookie = "userid="+userid;
  } else {
    cookie = "userid=0";
  }
  res.status(201);
  res.send(cookie);
  
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})