const mysql = require('mysql2');

const util = require('util');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '1234',
  database: 'mydatabase'
});

const queryAsync = util.promisify(connection.query).bind(connection);

connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL server: ' + err.stack);
    return;
  }

  console.log('Connected to MySQL server as id ' + connection.threadId);
});
async function wouldntdve() {
    await queryAsync(`create table Users (
        userid int not null auto_increment,
        username varchar(255),
        password varchar(255),
        primary key (userid)
    );`);

    await queryAsync(`insert into Users (username, password)
    values 
        ("abalcerak", "0710"),
        ("mjaved", "1234"),
        ("test", "abc");`);

    await queryAsync(
        `create table Addresses (
            id int not null auto_increment,
            userid int not null,
            country varchar(255),
            province varchar(255),
            city varchar(255),
            street varchar(255),
            postal varchar(255),
            primary key (id),
            foreign key (userid) references Users(userid)
        );`);
    connection.end();
}

wouldntdve();


