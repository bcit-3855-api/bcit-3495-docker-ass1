from flask import Flask, render_template
from pymongo import MongoClient
from werkzeug.urls import url_quote, url_unquote


app = Flask(__name__)

# Configure MongoDB connection
client = MongoClient('mongodb://host.docker.internal:27017/')
db = client['weather-storage']
collection = db['weather-information']

@app.route('/')
def index():
    # Fetch data from MongoDB
    data_from_mongo = collection.find()
    print(data_from_mongo)

    # Render HTML template with MongoDB data
    return render_template('index.html', data=data_from_mongo)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)