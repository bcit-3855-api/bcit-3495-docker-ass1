from pymongo import MongoClient

client = MongoClient('mongodb://host.docker.internal:27017/')
db = client['weather-storage']
collection = db['weather-information']

data = {
    "username": "test",
    "user_address": {"userid": 3, "country": "can", "province": "bc", "city": "poco", "street": "albert", "postal": "V3B"},
    'Current Weather Report': 'Sunny',
    'Weather Report for Upcoming Days': [
        {"day": 'Monday', "conditions": 'Cloudy', "temperature": 24, "Time": '2022-02-15T12:00:00.000Z'},
        {"day": 'Tuesday', "conditions": 'Rainy', "temperature": 20, "Time": '2022-02-15T12:00:00.000Z'}
    ]
}

collection.insert_one(data)
