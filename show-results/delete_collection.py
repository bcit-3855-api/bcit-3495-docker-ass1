import pymongo

# Create a MongoDB client
client = pymongo.MongoClient("mongodb://localhost:27017/")

# Access the database and collection
db = client["weather-storage"]
collection = db["weather-information"]

# Delete the collection
collection.drop()
